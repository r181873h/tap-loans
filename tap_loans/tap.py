"""Loans tap class."""

from pathlib import Path
from typing import List
import click
from singer_sdk import Tap, Stream
from singer_sdk.helpers.typing import (
    ArrayType,
    BooleanType,
    DateTimeType,
    IntegerType,
    NumberType,
    ObjectType,
    PropertiesList,
    Property,
    StringType,
)

from tap_loans.streams import (
    AssetsStream,
)

PLUGIN_NAME = "tap-loans"

STREAM_TYPES = [
    AssetsStream,
]


class TapLoans(Tap):
    """Loans tap class."""

    name = "tap-loans"
    config_jsonschema = PropertiesList(
        Property(
            "loans",
            ArrayType(
                ObjectType(
                    Property("name", StringType, required=True),
                    Property("start_date", DateTimeType, required=True),
                    Property(
                        "monthly_balances",
                        ArrayType(NumberType),
                        required=True,
                    ),
                )
            ),
            required=True,
        ),
        Property("start_date", DateTimeType),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


# CLI Execution:

cli = TapLoans.cli
